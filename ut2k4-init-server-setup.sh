#!/bin/bash
## Script for initial + security setup of Unreal Tournament 2004 Linux server. LinuxGSM handles the heavy stuff and other dependencies.

#Variable to check exit status, cleaner than dropping these lines everywhere.
checkok() {
            if [ $? -eq 0 ]; then
                :
        else
                echo -e "Error during installation/configuration, please check error messages"
                exit 1
        fi
}

#Setup firewall
sudo ufw default deny
sudo ufw allow http
sudo ufw allow https
sudo ufw allow ssh
sudo ufw allow 7777/udp
sudo ufw allow 7778/udp
sudo ufw allow 7787/udp
sudo ufw allow 7788/udp
sudo ufw allow 28902/tcp
sudo ufw allow 8080/tcp

sudo ufw allow XXXX
read

sudo ufw enable


# update system & enable i386 repo
sudo dpkg --add-architecture i386
checkok

sudo apt-get update && sudo apt-get install -y libsdl2-2.0-0:i386 && sudo apt-get remove --purge -y docker docker-ce docker-compose containerd.io docker-ce-cli docker-ce-rootless-extras docker-scan-plugin && sudo apt-get upgrade -y
checkok

## Fix this later to reflect based on current installed driver number/version using awk or something
sudo apt-get remove nvidia-driver-515
checkok

## Clean up and save space. if you're on a cloud service some charge by the hour for storage.
sudo apt-get autoremove -y
checkok

:<<COMMENT 
#Server Global Key: SRVER-9YPA9-FUPRA-8T3DQ

## add linux GSM stuff here. add key variable.

## GSM Install process. Read the rest here:
## https://linuxgsm.com/servers/ut2k4server/
wget -O linuxgsm.sh https://linuxgsm.sh && chmod +x linuxgsm.sh && bash linuxgsm.sh ut2k4server
read

./ut2k4server install

workdir=$(pwd)/ut2k4
mkdir -p "$workdir"

grep -v "MasterServerList" "$workdir"/serverfiles/System/ut2k4server.ini
sed -i 's|MasterServerList=(Address="utmaster.openspy.net",Port=28902)||g' serverfiles/System/ut2k4server.ini

for linuxGSM specifically, otherwise UT2004.ini, otherwise Default.ini
note that on the CLIENT side, for Linux specifically, you need to edit configs in $HOME/.ut2004/
https://www.pcgamingwiki.com/wiki/UT2004#Configuration_file.28s.29_location



[Engine.GameReplicationInfo]
ServerName=UT2004-Server-Name-CHANGEME
ShortName=SRVNMN
ServerRegion=0
AdminName=ADMIN
AdminEmail=
MessageOfTheDay=Hello!

[Engine.AccessControl]
AdminPassword=passpasspass

[IpDrv.MasterServerLink]
LANPort=11777 
LANServerPort=10777
MasterServerList=(Address="utmaster.openspy.net",Port=28902)

COMMENT


### Notes
## Admin Commands
## https://clients.orangelemon.nl/knowledgebase/32/Unreal-tournament-Admin-commands.html

## Server INI Reference
## https://www.unrealadmin.org/server_ini_reference/ut2004

##Extra sources. outdated or only useful for specific commands or notes.
## https://www.adamzwakk.com/guides/ut2004-server/
## https://wiki.unrealadmin.org/Server_Setup_(UT2004)
