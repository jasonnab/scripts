#!/bin/bash
## Script to generate a list of user uploads from an archive.org user page (which themselves are actually detailed "items"; the generated metadata of that "item" though does not appear to contain useful information).

## Usage: ./IAuserUploads.sh [username-here]
## simply drop the username as the first option

## Common Variables
checkok() {
	    if [ $? -eq 0 ]; then
		:
	else
		echo -e "Error during installation/configuration, please check error messages"
		exit 1
	fi
}

if [ "$EUID" -eq 0 ]; then
  echo "This script should be run as a normal user, not sudo or root."
  exit 1
fi

:<<'COMMENT'
COMMENT

## Variables
USER="$1"
	# Should have either a variable or array of entries corresponding to collections found on archive.org, that should be removed, because we never want to download entire collections, only users uploads that may exist WITHIN these collections

## Start of script

# Check for presense of lynx. all of thse checks are BROKEN until I find the right command
# https://stackoverflow.com/questions/592620/how-can-i-check-if-a-program-exists-from-a-bash-script

if "WHATEVER  "
	then
		:
	else
	read -p "lynx is required but not found. Would you like to attempt installation?" -n 1 -r
		if [[ ! $REPLY =~ ^[Yy]$ ]] ; then
		#, check for presense of apt/pacman/yum and use appropriate command
			if [z = apt] ; then
				# pretty sure setting a variable here will not allow it to get out of this loop 
				PKGVAR= "apt update && apt install lynx"
			elif [z = pacman] ; then
				PKGVAR = "pacman -Syy lynx"
			elif [z = yum ] ; then
				PKGVAR = "yum WHATEVER lynx"
			fi
		"$PKGVAR" 
		else
			echo "Unable to determine your package manager,\n or unable to install lynx.\nPlease install manually"
			exit 1
		fi

# make user directory, change to it. 
mkdir "$USER" && pushd ./"$USER"

# grab a list of URLs using Lynx
lynx -listonly -nonumbers -dump "https://archive.org/details/@$USER?tab=uploads" >> "$USER-filelist.txt"

# Backup, sort, rename the filelist
cp "$USER-filelist.txt" "$USER-filelist.bak.txt" &&
sort "$USER-filelist.txt" >> "$USER-filelist..txt" && 
mv "$USER-filelist..txt" "$USER-filelist.txt"

## Now the big part. Grepping or sedding out specific lines of text. See the ## Variables section relating to this.
# TBD

# Finally, remove https://archive.org/details , because ia download only expects the actual details item, because each is unique.
	## uhh not sure double quotes are acceptable here, revisit. only bc im lazy to slash the slashes to make them not slash as sed would slash a slash""""
sed -i "s|https://archive.org/details||g" "$USER-filelist.txt"
