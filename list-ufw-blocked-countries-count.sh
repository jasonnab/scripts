#!/bin/bash
# Stupid complicated script that has too many pipes, to check how many countries have been blocked by firewall



BF="block.txt"
BCF="block-count.txt"

# Get the blocked IPs
sudo journalctl | grep -e '\[UFW BLOCK\]' | awk '{printf "%s\n",$11}' | sed 's|SRC\=||g' | sort | uniq > "$BF"

# Whois check country
while read i ; do 
	sleep 1s ;
	whois "$i" | grep -i -e country | grep -v 'ZZ' | sed 's|[Cc]ountry:        |country: |g' | uniq 2>&1 | tee -a "$BCF" ;
	done<"$BF"

# Pretty sort the output
sort -h "$BCF" | uniq -c | sort -h
