#!/bin/bash
## info (on) System (that's) Unknown Script
## aka infoSUS.sh ;)

#https://serverfault.com/questions/103501/how-can-i-fully-log-all-bash-scripts-actions
exec 3>&1 4>&2
trap 'exec 2>&4 1>&3' 0 1 2 3
### Need to add a check if we have write permissions to /dev/shm; if it exists as well
### Alternatives are /tmp, /run, $(pwd), $HOME
exec 1>>/dev/shm/infoSUS.sh.log 2>&1

## Check for sudo rights
# Alternatively make it a script run flag, -su or something
if [ "$EUID" -ne 0 ]
  then echo "Script not run with sudo or as root, testing if you have sudo rights regardless"
  exit
fi

alias EB='echo ""'

## Get current logged in users
w -si ; EB

## Get last logins
last | head -n 16 ; EB

## Get mounted or mapped drive available space. Useful to know if a drive
## is full, which maybe a cause of problems in system functionality.
df -h ; EB

## Check all available block devices, pci devices, USB devices
lsblk ; EB
lspci ; EB
lsusb ; EB

## Export dmesg if we have root rights

# if sudoyes flag equal Y or On; then
#	sudo dmesg -T ; EB
# fi

## Check if we're using systemd, then continue BELOW commands

## Export Journalctl for today
## If we have sudo rights, run that for journalctl otherwise it only
## exports user-level journalctl stuff

# if sudoyes flag equal Y or On; then
#	sudo journalctl --since today ; EB
#	else
#	journalctl --since today ; EB
# fi


## Get installed package list
## Check for apt, emerge, pacman, yum, etc; run appropriate command based on installed thing
## maybe extend for NixOS, Bedrock Linux, or Rocky Linux
