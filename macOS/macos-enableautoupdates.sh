#!/bin/bash
if [ "$EUID" -ne 0 ]
  then echo "This script must be run as root or using sudo"
  exit
fi

#https://serverfault.com/questions/103501/how-can-i-fully-log-all-bash-scripts-actions
exec 3>&1 4>&2
trap 'exec 2>&4 1>&3' 0 1 2 3
exec 1>>/var/log/softwareupdate.out 2>&1

checkok() {
	    if [ $? -eq 0 ]; then
		:
	else
		echo -e "Error during installation/configuration, please check error messages"
		exit 1
	fi
}

date
echo "Enabling auto update checks"
softwareupdate --schedule on
checkok

echo "Enabling auto update checks - backup"
/usr/bin/defaults write /Library/Preferences/com.apple.SoftwareUpdate.plist AutomaticCheckEnabled -bool TRUE
checkok

echo "Enabling auto update downloads"
/usr/bin/defaults write /Library/Preferences/com.apple.SoftwareUpdate.plist AutomaticDownload -bool TRUE
checkok

#Disabled as we don't necessarily want major updates being installed automatically
#echo "Enabling auto MacOS update installs"
#/usr/bin/defaults write /Library/Preferences/com.apple.SoftwareUpdate.plist AutomaticallyInstallMacOSUpdates -bool TRUE
#checkok

echo "Enabling auto config data installs"
/usr/bin/defaults write /Library/Preferences/com.apple.SoftwareUpdate.plist ConfigDataInstall -bool TRUE
checkok

echo "Enabling auto critical update installs"
/usr/bin/defaults write /Library/Preferences/com.apple.SoftwareUpdate.plist CriticalUpdateInstall -bool TRUE
checkok

# Enable installing app updates for App Store stuff
#/usr/bin/defaults write /Library/Preferences/com.apple.commerce.plist AutoUpdate -bool TRUE

