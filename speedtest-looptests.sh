#!/bin/bash
## This is for speedtest-cli
## git clone https://github.com/sivel/speedtest-cli && cd speedtest-cli

# Variables
DATE=$(date +%Y-%m-%d_%H.%M)
LOG="test_$DATE.log"

## Get a list of servers first, using speedtest-cli --list and selecting
## your desired servers.
### NOTICE! Servers can disappear from test to test. Not sure why, but
### speedtest-cli is checking the Speedtest API, so something must change.
SRVS="servers.txt"

## This is a python virtual-environment
## python3 -m venv speedtest
# Deactivate any existing virtual-environment first
deactivate
source ./speedtest/bin/activate

#Declare variable to store server IDs
declare -a server_array
for s in $(cat "$SRVS"); do server_array+=("$s") ; done

#Get the exact date
date | tee -a "$LOG"

#Run through array
for SRVID in "${server_array[@]}"; do
echo "We are testing server ID: $SRVID" | tee -a "$LOG"
speedtest-cli --server "$SRVID" 2>&1 | tee -a "$LOG"
done
