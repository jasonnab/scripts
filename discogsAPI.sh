#!/bin/bash
# Script to manage your Discogs collection from command line
############################################################
####### # IN PROGRESS # IN PROGRESS # IN PROGRESS ##########
############################################################

# Variables

## !! Do not modify this please !! ##
UA="discogsAPI.sh/1.0 email/git-AT-nabein-DOT-me"

## Only because Discogs may change the URL in the future
API_URL="https://api.discogs.com/"
#Your account API personal access token 
TOKEN=""
#your account user ID
USER=""

## Right now the input file should only have IDs of releases
## you want to add, no URLs
INPUT=""

# values right now are "wants" or "collection"
SECTION=

# Process enables
LISTID='off'
RLSNAME='off'



# Run the process in a loop
while read i ; do 
	# Enable listing the ID
	if [ "$LISTID" = 'on' ] ; then
		#DO STUFF HERE!!!
	else
		:
	fi
	
	# Enable pulling the ID release name and listing it
	if [ "$RLSNAME" = 'on' ] ; then
	## Bleh this is a mess. I want the JSON data stored in memory only, could use /dev/shm for a temporary text file
	## 
	RLSJSON="$(curl "$API_URL/releases/$i" --user-agent "$UA")"
	
	# https://stackoverflow.com/questions/36073695/how-to-retrieve-single-value-with-grep-from-json/36075904#36075904	
	# grep artist name (only the first?) and release title.
	RLSNAME="$(grep -oP '(?<="name": ")[^"]*' "$RLSJSON" | head -n 1)"
	RLSTITLE="$(grep -oP '(?<="title": ")[^"]*' "$RLSJSON" | head -n 1)"
	echo Processing release: "$RLSNAME" - "$RLSTITLE"
	sleep 1s
	else
		:
	fi
	
	curl "$API_URL/users/$USER/$SECTION/$i" -X PUT -H "Authorization: Discogs token=$TOKEN" --user-agent "$UA"
	# Don't hammer the discogs servers
	sleep 2s 
done<"$INPUT"
