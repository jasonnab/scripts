#!/bin/bash
# Script to make folders accessible for cgit
# Usage: mvgithttp.sh "foldername"

### Recommended rsync:
## rsync -rltvDP --keep-dirlinks $FOLDER server:"$HOMEDIR"

### https://stackoverflow.com/questions/2816369/git-push-error-remote-rejected-master-master-branch-is-currently-checked
### Alternatively, if you're not editing on your cgit server:
## cd "$1" && git config --local receive.denyCurrentBranch updateInstead
### on host:
## git push SERVER:"$DEST"/"$1"


DEST="/var/www/git.nabein.me/"
HOMEDIR="$HOME/git"

### For this to work cleanly... 
### I am using fcgiwrap and nginx. both run as http
## 
## sudo usermod -a -G http "$YOUR_USERNAME"
## sudo chmod 775 "$DEST"
## sudo chown http:http "$DEST"
##
### Do this for every new git folder you add.
## sudo chmod 775 "$1"
## sudo chown http:http "$1"
##
### Alternatively, you can give http permission to use sftp or ssh for updates.

# Move the desired folder to the dest folder that http can access
sudo mv "$1" "$DEST"

# Change ownership to http recursively. 
# If cgit/fcgi can't read .git details, it won't render information properly.
sudo chown -R http:http "$DEST"/"$1"

# Change permission to allow http user AND group write access; optional only if your project will continue to get git updates
# if your git project is static, comment this out.
sudo chmod 775 "$DEST"/"$1"

#Change to allow folders to be written
sudo find "$DEST"/"$1"/.git/ -type d -exec chmod 775 {} +
# Change to allow files to be written
sudo find "$DEST"/"$1"/.git/ -type f -exec chmod 664 {} +

#sudo chmod 775 "$DEST"/"$1"/.git/

# Make a softlink for easy access
ln -s "$DEST"/"$1" "$HOMEDIR"

# tada
