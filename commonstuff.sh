#!/bin/bash

#Variable to check exit status, cleaner than dropping these lines everywhere.
checkok() {
	    if [ $? -eq 0 ]; then
		:
	else
		echo -e "Error during installation/configuration, please check error messages"
		exit 1
	fi
}

#Check if we are root or running with sudo, if not, fail and exit.
if [ "$EUID" -ne 0 ]
  then echo "This script must be run as root or using sudo"
  exit 1
fi

# This script is intended to be run by normal users
if [ "$EUID" -eq 0 ]; then
  echo "This script should be run as a normal user, not sudo or root."
  exit 1
fi

##Help command##
usage="$(basename "$0") [-h|--help] Whatever
-h|--help: Show this help text

Text goes here."

#Really simple help dialog check
if [ "$1" == "-h" ]; then
  echo "$usage"
  exit 0
elif [ "$1" == "--help" ]; then
  echo "$usage"
  exit 0
fi

#How to do a block of comments
:<<'COMMENT'
COMMENT

#Function from: https://askubuntu.com/a/599848 / Function for file selection.
function choose() {
  PFXPATH="$(zenity --file-selection --title='Select your file' 2>/dev/null)"
  case $? in
           0)
                  return;;
           1)
                  zenity --question \
                         --title="File Selection" \
                         --text="Do you want to select a file?" 2>/dev/null \
                         && choose || exit;;
          -1)
                  echo -e "An unexpected error has occurred.\nPress enter to exit"; read; exit;;
  esac
}

## yes/no prompt
read -p "QUESTION_HERE" -n 1 -r
echo
if [[ ! $REPLY =~ ^[Yy]$ ]]
then
	"DO_STUFF"
else
	"DO_OTHER_STUFF"
fi


## Check command existance
## https://stackoverflow.com/questions/592620/how-can-i-check-if-a-program-exists-from-a-bash-script
#POSIX compatible:

command -v <the_command>

# Example use:

if ! command -v <the_command> &> /dev/null
then
    echo "<the_command> could not be found"
    exit
fi

#For Bash specific environments:

hash <the_command> # For regular commands. Or...
type <the_command> # To check built-ins and keywords
