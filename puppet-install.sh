#!/bin/zsh
# Install the puppet agent. For Mac OSX

### To be updated for Linux as well, which is why it's not in the MacOS folder ###

#OSX 10.15 results in 10.15
#OSX 11 results in 11.X

MACVER=$(sw_Vers | grep ProductVersion |awk '{printf $2}'|cut -b1-5)
PUPMASTER=""

### DISABLED TO AVOID RUNNING CURL AS ROOT ###
#Check if we are root or running with sudo, if not, fail and exit.
#if [ "$EUID" -ne 0 ]
#  then echo "This script must be run as root or using sudo"
#  exit
#fi
###

checkok() {
	    if [ $? -eq 0 ]; then
		:
	else
		echo -e "Error during installation/configuration, please check error messages"
		exit 1
	fi
}


# Download the repo file - Puppet v6
## For MacOS 12 and Puppet agent v6, there doesn't seem to be a specific build folder. Version 11 has been tested and appears to work fine.
if [ $(echo "$MACVER"|cut -b 1-2) -eq 12 ]; then
	curl -s "https://downloads.puppetlabs.com/mac/puppet6/11/x86_64/puppet-agent-latest.dmg" -L -O
	checkok
elif [ $(echo "$MACVER"|cut -b 1-2) -eq 11 ]; then
	curl -s "https://downloads.puppetlabs.com/mac/puppet6/11/x86_64/puppet-agent-latest.dmg" -L -O
	checkok
elif [ $(echo "$MACVER"|cut -b 1-2) -eq 10 ]; then	
	curl -s https://downloads.puppetlabs.com/mac/puppet6/"$MACVER"/x86_64/puppet-agent-latest.dmg -L -O
	checkok
else
	echo "We can't determine your Mac version; or, you're using an older or unsupported version."
	echo "Please contact your IT Support desk for further manual assistance"
	echo "Your MacOS Version is: $MACVER"
	echo "Press enter to exit..."
	read
	exit 1
fi

# Attach the dmg and process install.
# Use find to get the path of the .dmg.
echo "Please enter your user account password if prompted. You must have admin rights for this to be successful"
sudo hdiutil attach ./puppet-agent-latest.dmg
	checkok
PUPPATH=$(find /Volumes/puppet*/ -type f)

sudo installer -pkg "$PUPPATH" -target /.
	checkok

sudo hdiutil unmount "$PUPPATH"
rm -f ./puppet-agent-latest.dmg

# Point it at our server
sudo /opt/puppetlabs/bin/puppet config set server "$PUPMASTER" --section main
	checkok

# Ensure it's installed and running
sudo /opt/puppetlabs/bin/puppet resource service puppet ensure=running enable=true
	checkok
