#!/bin/bash

#Set the server IP or name as assumingly found in $HOME/.ssh/config
SERVER=sx134

#Start the remote port forwarding ssh connection
#Don't open a remote shell, fork process to background
#Forward remote port 8384 to local port 8385, because attempting to run syncthing on your local device will cause a port conflict/access denied

## ADD A CHECK IF AN EXISTING PORT FORWARD EXISTS AND OFFER TO KILL IT ##
## Alternatively, it would be wise to build in an if statement or use control sockets to check and exit the ssh connection after a specific set of time.
## However, when sending an xdg-open, this sort of "forks" the browser process, since it simply sends the command to the browser, rather than running an entire browser setup.
## perhaps if it's possible to read the xdg-open default that's set, we could set a variable for the browser and simply launch that way; if the browser is open however I believe this will still just "fork" control to the browser rather than running an entire new process.

## further reference
## https://stackoverflow.com/a/38097821
## https://stackoverflow.com/a/26470428

ssh -N -L 8385:localhost:8384 "$SERVER" & 2>&1 >>/dev/null

#Sleep 5s to account for ssh connection time. You may lower this
sleep 5s

#Open your default browser to the syncthing admin page
xdg-open http://localhost:8385

## do something like if browser open then wait else exit?
