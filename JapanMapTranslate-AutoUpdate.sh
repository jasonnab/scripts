#!/bin/bash
#################################################################
## name: JapanMapTranslate AutoUpdate                           #
## description: Auto-gen process for updated JapanMapTranslate  #
##              .obf's, based on latest Japan OSM map tiles     #
## author: jason nabein                                         #
## website: https://git.nabein.me                               #
## license: public domain/do whatever you want license          #
#################################################################

### TODO IMPLEMENT STORAGE AVAILABILITY CHECK
### Something like "if $(df --output=avail . | sed 1d) >= 107374182400 ; then echo "warning etc""
### 107374182400 is 100GB in 1024 >byte< chunks
### and that df output has to match properly, with regards to SI notation/whatever

#### WARNING ####
## you need APPROXIMATELY 100GB to complete this script automated.
## ~3GB for bz2 files, 
## ~40GB for the extracted original untranslated .osm files, 
## ~50GB for the translated files (as more text is used)
## ~3GB for final .obf files. 
## Also, ~15GB temp file used while making the specific .obf file.
##
## Those temp files are deleted after each obf is generated.
##
## The extracted untranslated .osm files + .bz2's aren't deleted 
## due to taking a while to extract, and download initially.

## Pre-start space check ##
## TBD
## End Pre-start space check

## Start Variables
MAP_SOURCE="https://download.geofabrik.de/asia/japan/"
APP_SOURCE="https://github.com/rrobek/JapanMapTranslate"
APP_FALLBACK_SOURCE="https://rrobek.de/osm/JapanMapTranslate-1.3.zip"
OSMC_SOURCE="https://download.osmand.net/latest-night-build/OsmAndMapCreator-main.zip"
FALLBACK_FN="JapanMapTranslate-1.3.zip"

#Function to check exit status, cleaner than dropping these lines everywhere.
checkok() {
            if [ $? -eq 0 ]; then
                :
        else
				echo "Last command run was ["!:0"] with arguments ["!:*"]"
                echo "Error during process, please check last error messages"
                exit 1
        fi
}

## End Variables


## Start Pre-Check for Java
if ! command -v "java" &> /dev/null
then
	echo "Java is missing. Please confirm your \$PATH contains the path for the java binary, or install Java from your package manager."
	echo "Javaが見つかりません。\$PATHにjavaのバイナリのパスが含まれていることを確認するか、パッケージマネージャからJavaをインストールしてください。"
	exit 1
else
	:
fi

## Start Script

# Download the JapanTranslate app #

# git pull from the open source repo to reduce traffic on the personal website
# but fallback if needed
if ! command -v "git" &> /dev/null
then
	"wget" "$APP_FALLBACK_SOURCE"
	checkok
	export FALLBACK=1
	
else
	if [ -d "JapanMapTranslate" ];
		then
			:
		else
	
	"git" clone "$APP_SOURCE"
	checkok
	
		fi
fi

# Download Maps #

# Check for aria2c otherwise use wget
## TODO cleanup so that the prefectures are stored in a variable. Struggling a little bit
## to understand how to properly expand that FROM the variable into individual entries. while loop? idk
## In a variable we can reuse it later
for PREFECTURE in {chubu,chugoku,hokkaido,kansai,kanto,kyushu,shikoku,tohoku} ; do

	if ! command -v "aria2c" &> /dev/null
	then
		# this is gonna be slow
		"wget" "$MAP_SOURCE"/"$PREFECTURE-latest.osm.bz2" -O "$PREFECTURE-latest.osm.bz2"
		checkok
	else		
		"aria2c" -x 4 -s 4 --continue=true "$MAP_SOURCE"/"$PREFECTURE-latest.osm.bz2"
		checkok
	fi

#clear vars to be safe
export PREFECTURE=
done

# Extract Packages. This takes a while #

# Check for 7z, otherwise use bzip2. 
# 7z has more verbose output, more progress details. No speed increase AFAIK.

# Loop through each archive
for i in *-latest.osm.bz2 ; do

	if ! command -v "7z" &> /dev/null
	then
		"bzip2" -v -f -d "$i"
		checkok
	else
		### TODO
		# This should NOT require user input if the file already exists; it should assume the file is extracted.
		# As in, it prompts to overwrite or Skip, which in test cases we should Skip
		# It should rather overwrite just to be safe.
		# However, this script prompts to clean up temp files after successful completion, so realistically it shouldn't matter
		"7z" x "$i"
		checkok
	fi

#clear vars to be safe
export i=
done

# Replace the text in each map file #

# This part takes a while. I believe the application is not 
# multi-threaded, or if it is, it's limited to 2 CPU threads... 
# I'm not a Java dev so...

for i in *-latest.osm ; do
	
	if [[ $FALLBACK = 1 ]] ;
	then
		unzip "$FALLBACK_FN" -d "JapanMapTranslate-1.3"
		checkok
		"./JapanMapTranslate-1.3/JapanMapTranslate.sh" "$i"
		checkok
	else
		chmod +x "./JapanMapTranslate/bin/JapanMapTranslate.sh"
		"./JapanMapTranslate/bin/JapanMapTranslate.sh" "$i"
		checkok
	fi

#clear vars to be safe	
export i=	
done


# Download OSMANDMC to convert to OBF #
#
# This takes a long time, 4hr+ depending on CPU, RAM, Disk speed
# OSMANDMC again appears to be only use 2 CPU threads though...
# Give it at least 8GB of RAM but don't be surprised if you need 14GB+.
# Default to 16GB

	wget "$OSMC_SOURCE"
	checkok
	unzip -q "OsmAndMapCreator-main.zip" -d OsmAndMapCreator
	checkok
	
	# Edit memory allocation. Change Xms16G as desired
	#
	# '--add-opens java.base/java.util=ALL-UNNAMED' required until project is rebased to Java 17+
	#
	# See: https://github.com/osmandapp/OsmAnd-tools/issues/484#issuecomment-1315382705 
	# or,
	# See: https://github.com/osmandapp/OsmAnd-tools/issues/334#issuecomment-986530101
	#
	# Confirmed working with OpenJDK build 19.0.1+10 (Arch Linux local/jre-openjdk-headless 19.0.1.u10-3)
	

	sed -i 's|Xmx512M|Xmx16G --add-opens java.base/java.util=ALL-UNNAMED|g' OsmAndMapCreator/utilities.sh
	
	## START_BUILD
for i in *-latest.osm.tr.osm ; do

:<<'TODO'	

	# TODO - Check for existing .obf files. Set a var? then prompt to overwrite files if the var is set
	# rough draft below, remove TODO section comments to enable	

for i in {Chubu,Chugoku,Hokkaido,Kansai,Kanto,Kyushu,Shikoku,Tohoku} ; do
	if [ -f "$i".obf ]; then
		echo "$i exists."
		export AN_OBF_EXISTS = 1
	fi
	
	if [[ $AN_OBF_EXISTS = 1 ]] ; then
		read -p "Would you like to continue generating new files? Y/N" -n 1 -r
		echo
			if [[ $REPLY =~ ^[Yy]$ ]]; then
				:
			else
				echo "OK. Stopping build process."
				echo "Run build commands manually if desired."
				echo "Please check the script in section '## START_BUILD' for more information."
				exit 1
			fi
			
		else
		:
	fi
TODO
	# CLI based on: https://docs.osmand.net/docs/technical/map-creation/create-offline-maps-yourself/#vector-maps-shell-script
	
	OsmAndMapCreator/utilities.sh generate-obf "$i" --chars-build-poi-nameindex=3 2>&1 | tee -a "$i".log
	checkok
done

# Rename final files to be compliant with OSMAnd+ #
## Set the date because the day may change while compiling, but it's the maps that are important for dating.
DATE="$(date +%F)"
DIR="complete-export-$DATE"

mkdir "$DIR" >/dev/null

mv Chubu-latest.obf  	"$DIR"/Japan_chubu_asia.obf
mv Chugoku-latest.obf  	"$DIR"/Japan_chugoku_asia.obf
mv Hokkaido-latest.obf	"$DIR"/Japan_hokkaido_asia.obf
#Naming is correct, see: https://en.wikipedia.org/wiki/Kansai_region#Name
mv Kansai-latest.obf 	"$DIR"/Japan_kinki_asia.obf
mv Kanto-latest.obf 	"$DIR"/Japan_kanto_asia.obf
mv Kyushu-latest.obf	"$DIR"/Japan_kyushu_asia.obf
mv Shikoku-latest.obf	"$DIR"/Japan_shikoku_asia.obf
mv Tohoku-latest.obf	"$DIR"/Japan_tohoku_asia.obf

# Touch the date in the folder so that it's known when the buildtime was.
# The OBF files also contain the date, but if you maintain multiple copies 
# of build files across various timeframes or folders, it would be helpful
# to have this I feel.
touch "$DIR"/built-"$DATE"

# List all the files just to be sure it's OK
ls "$DIR"/Japan_{chubu,chugoku,hokkaido,kinki,kanto,kyushu,shikoku,tohoku}_asia.obf
echo "Confirm all required files are generated and renamed properly"
echo "Otherwise, rerun manually as needed"



# Cleanup Section #
## Why separate them? idk. I'm sure I can do a case/multi-selection
## but better to do it cleaner for documentation purposes.
## It could also be done with function for the read/etc but whatever

# Cleanup Files #
echo
read -p "Would you like to cleanup temp map files? Y/N: " -n 1 -r
if [[ $REPLY =~ ^[Yy]$ ]]
then
	echo
	echo "\nDeleting .osm & .bz2 files"
	rm -vf ./*.osm*
else
	:
fi

# Cleanup Software #
echo
read -p "Would you like to cleanup build software? Y/N: " -n 1 -r
if [[ $REPLY =~ ^[Yy]$ ]]
then
	echo
	echo "Deleting OsmAndMapCreator"
	rm -rvf OsmAndMapCreator OsmAndMapCreator-main.zip regions.ocbf
	echo
	echo "Deleted JapanMapTranslate"
	rm -rvf JapanMapTranslate JapanMapTranslate-1.3.zip
else
	:
fi

# Package .obf files into compressed zip. This is optional, but helpful for transfer to mobile devices #
# Uncomment below to enable. Change 9 for different level of compression. #
# time zip -9r ./"$DIR".zip ./"$DIR"

echo 
echo "Build complete! Copy the files to your Storage folder for OsmAnd."
echo "Review JapanMapTranslate website for detailed use information:"
echo 
echo "https://rrobek.de/jmtranslate.html"
echo "https://rrobek.de/japanmaps.html"

exit 0
