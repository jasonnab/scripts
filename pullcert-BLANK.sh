#!/bin/bash
# Script to import and trust a required cert chain.
# Based around Ubuntu 18/20 with regards to cert folder locations and updating ca-certs.
HOST=""
PORTNUMBER="443"
SERVERNAME=""
OUTPUT="/usr/local/share/ca-certificates"

#Check if we are root or running with sudo, if not, fail and exit.
####Script should probably be rewritten to only require sudo for system related tasks; ie. avoid using curl as root####
if [ "$EUID" -ne 0 ]
  then echo "This script must be run as root or using sudo"
  exit
fi

checkok() {
	    if [ $? -eq 0 ]; then
		:
	else
		echo -e "Error during installation/configuration, please check error messages"
		exit 1
	fi
}

#https://superuser.com/questions/97201/how-to-save-a-remote-server-ssl-certificate-locally-as-a-file
echo -e "\nPROGRESS: Grabbing certificate from website"
echo -n | openssl s_client -connect "$HOST":"$PORTNUMBER" -servername "$SERVERNAME" | openssl x509 > "$OUTPUT"/"$SERVERNAME".crt

for link in "$OUTPUT"/*.crt ; do
	pushd /etc/ssl/certs 2>&1>>/dev/null
	echo -e "\nPROGRESS: Linking cert"
	ln -s "$link" 2>/dev/null 
	echo -e "\nPROGRESS: Return to folder"
	cd "$(dirs -l -0)" && dirs -c
done

echo -e "\nPROGRESS: Updating certs"
update-ca-certificates 2>/dev/null 1>/dev/null
checkok
clear

echo -e "\nPROGRESS: Showing certificate paths"
ls -al "$OUTPUT"/"$SERVERNAME".crt
ls -al /etc/ssl/certs/"$SERVERNAME".{crt,pem}
echo -e "\nProcess complete, paths to cert should be shown above"
exit 0
